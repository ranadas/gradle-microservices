package com.rdas.model

import groovy.transform.ToString
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

/**
 * Created by rdas on 26/08/2015.
 */
@ToString(includeNames=true, includeFields=true)
@Document(collection = "contacts")
class Contact {
    @Id
    String id;
    String firstName;
    String lastName;
    String address;
    String phoneNumber;
    String email;
    String twitterHandle;
    String facebookProfile;
    String linkedInProfile;
    String googlePlusProfile;

//    String getId() {
//        return id
//    }
//
//    void setId(String id) {
//        this.id = id
//    }
//
//    String getFirstName() {
//        return firstName
//    }
//
//    void setFirstName(String firstName) {
//        this.firstName = firstName
//    }
//
//    String getLastName() {
//        return lastName
//    }
//
//    void setLastName(String lastName) {
//        this.lastName = lastName
//    }
//
//    String getAddress() {
//        return address
//    }
//
//    void setAddress(String address) {
//        this.address = address
//    }
//
//    String getPhoneNumber() {
//        return phoneNumber
//    }
//
//    void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber
//    }
//
//    String getEmail() {
//        return email
//    }
//
//    void setEmail(String email) {
//        this.email = email
//    }
//
//    String getTwitterHandle() {
//        return twitterHandle
//    }
//
//    void setTwitterHandle(String twitterHandle) {
//        this.twitterHandle = twitterHandle
//    }
//
//    String getFacebookProfile() {
//        return facebookProfile
//    }
//
//    void setFacebookProfile(String facebookProfile) {
//        this.facebookProfile = facebookProfile
//    }
//
//    String getLinkedInProfile() {
//        return linkedInProfile
//    }
//
//    void setLinkedInProfile(String linkedInProfile) {
//        this.linkedInProfile = linkedInProfile
//    }
//
//    String getGooglePlusProfile() {
//        return googlePlusProfile
//    }
//
//    void setGooglePlusProfile(String googlePlusProfile) {
//        this.googlePlusProfile = googlePlusProfile
//    }
}