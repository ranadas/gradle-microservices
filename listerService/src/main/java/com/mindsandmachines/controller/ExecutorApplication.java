package com.mindsandmachines.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

/**
 * Created by rdas on 24/08/2015.
 */
@ComponentScan("com.mindsandmachines.services")
@SpringBootApplication
@EnableScheduling
public class ExecutorApplication {

    private static final Logger logger = LoggerFactory.getLogger(ExecutorApplication.class);

    @PostConstruct
    void testPools() {
        logger.info("\n\n *******PostConstruct\n\n");
    }
    public static void main(String[] args) throws Exception {
        SpringApplication.run(ExecutorApplication.class, args);
    }
}
