package com.mindsandmachines.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import javax.servlet.MultipartConfigElement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
/**
 * Created by rdas on 24/08/2015.
 */
@EnableSwagger2
@ComponentScan("com.mindsandmachines.controller")
@SpringBootApplication
public class BootApplicationExecutor {

    private static final Logger logger = LoggerFactory.getLogger(BootApplicationExecutor.class);

    @PostConstruct
    void testPools() {
        logger.info("\n\n ****** REST service PostConstruct\n\n");
    }

    public static void main(String[] args) throws Exception {


        /*
        Options options = new Options();
        options.addOption("h", "host", true, "Server hostname (default: localhost)");
        options.addOption("p", "port", true, "Server port (default: 3000)");

        // parse the command line args
//        CommandLineParser parser = new PosixParser();
        CommandLineParser parser = new BasicParser();
//        CommandLine cl = parser.parse(options, args, false);
        CommandLine cl = parser.parse(options, args);

        // set properties
        Properties as = System.getProperties();
        String host = cl.getOptionValue("h", "localhost");
        as.put("seedHost", host);
        String portString = cl.getOptionValue("p", "3000");
        as.put("port", portString);
        */

        SpringApplication.run(BootApplicationExecutor.class, args);
    }


    @Bean
    public MultipartConfigElement multipartConfigElement() {
        return new MultipartConfigElement("");
    }

}
