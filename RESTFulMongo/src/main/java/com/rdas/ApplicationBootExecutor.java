package com.rdas;

import com.rdas.configuration.TodoConfig;
import com.rdas.model.Account;
import com.rdas.repo.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;

/**
 * Created by rdas on 24/08/2015.
 */
//@ComponentScan({"com.rdas.controller"})
//@ComponentScan
@EnableSwagger2
@PropertySource("classpath:/application.properties")
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
@Import({TodoConfig.class})
public class ApplicationBootExecutor {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationBootExecutor.class);

    @PostConstruct
    void initialise() {
        logger.info("\n\n ****** REST service PostConstruct\n\n");
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(ApplicationBootExecutor.class, args);
    }

    /*
    @Bean
    CommandLineRunner init(final AccountRepository accountRepository) {
        return new CommandLineRunner() {
            @Override
            public void run(String... arg0) throws Exception {
                accountRepository.save(new Account("rbaxter", "password"));
                accountRepository.save(new Account("rdas", "pass"));
            }
        };
    }

    @Configuration
    class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

        @Autowired
        AccountRepository accountRepository;

        @Override
        public void init(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userDetailsService());
        }

        @Bean
        UserDetailsService userDetailsService() {
            return new UserDetailsService() {

                @Override
                public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
                    Account account = accountRepository.findByUsername(username);
                    if (account != null) {
                        return new User(account.getUsername(), account.getPassword(), true, true, true, true,
                                AuthorityUtils.createAuthorityList("ROLE_USER"));
                    } else {
                        throw new UsernameNotFoundException("could not find the user '"
                                + username + "'");
                    }
                }

            };
        }
    }

    @EnableWebSecurity
    @Configuration
    class WebSecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests().anyRequest().fullyAuthenticated().and().
                    httpBasic().and().
                    csrf().disable();
        }
    }
    */
}
