package com.mindsandmachines.controller

import io.swagger.annotations.Api
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

/**
 * Created by rdas on 25/08/2015.
 */
@Api(value = "FileUploadController", description = "Operations about Uploading Files")
@RestController
class FileUploadController {

    private static final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

    @RequestMapping(value = "/addpics", method = RequestMethod.GET)
    public @ResponseBody
    String provideUploadInfo() {
        return "You can upload file by POSTing to the same URL."
    }

    /**
     * curl -i -X POST -H "Content-Type: multipart/form-data" -F "file=@pic.jpg;name=pic.jpgs" http://localhost:8080/addpics?name=blabla.jpg
     * @param name
     * @param file
     * @return
     */
    @RequestMapping(value = "/addpics", method = RequestMethod.POST)
    public @ResponseBody
    String uploadPics(@RequestParam(value = "name", required = false) String name,
                      @RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            logger.info("Failed to upload " + name + " because the file was empty.")
            return "You failed to upload " + name + " because the file was empty."
        } else {
            String originalFilename = file.getOriginalFilename();
            byte[] bytes = file.getBytes()
            BufferedOutputStream stream =
                    new BufferedOutputStream(new FileOutputStream(new File(originalFilename)))
            stream.write(bytes)
            stream.close()

            logger.info("Successfully uploaded $name")
            return "You successfully uploaded $name"
        }
    }
}
