package com.rdas.todo;

import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by rdas on 27/08/2015.
 */
public interface TodoRepository extends Repository<ToDoTask, String> {

    void delete(ToDoTask deleted);

    List<ToDoTask> findAll();

    Optional<ToDoTask> findOne(String id);

    ToDoTask save(ToDoTask saved);
}