package com.rdas.repo

import com.rdas.model.Account
import org.springframework.data.mongodb.repository.MongoRepository;
/**
 * Created by rdas on 28/08/2015.
 */
public interface AccountRepository extends MongoRepository<Account, String> {
    public Account findByUsername(String username);
}