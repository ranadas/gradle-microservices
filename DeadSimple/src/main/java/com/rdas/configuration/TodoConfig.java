package com.rdas.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rdas on 27/08/2015.
 */
@ComponentScan({"comxxx.javaadvent.bootrest.*", "com.rdas.controller"})
//@ComponentScan
@Configuration
@EnableAutoConfiguration
public class TodoConfig {
}
