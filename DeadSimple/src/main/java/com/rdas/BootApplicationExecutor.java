package com.rdas;

import com.javaadvent.bootrest.TodoAppConfig;
import com.rdas.configuration.TodoConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;

/**
 * Created by rdas on 24/08/2015.
 */
//@ComponentScan
@EnableSwagger2
@SpringBootApplication
//@Configuration
//@EnableAutoConfiguration
@Import({TodoConfig.class, TodoAppConfig.class})
//@ComponentScan({"com.rdas.configuration.*"})
public class BootApplicationExecutor {

    private static final Logger logger = LoggerFactory.getLogger(BootApplicationExecutor.class);

    @PostConstruct
    void testPools() {
        logger.info("\n\n ****** DeadSimple REST service PostConstruct\n\n");
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BootApplicationExecutor.class, args);
    }
}
