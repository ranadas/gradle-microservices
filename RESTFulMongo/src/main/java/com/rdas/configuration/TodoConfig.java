package com.rdas.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by rdas on 27/08/2015.
 */
@Configuration
@ComponentScan({"com.rdas.controller", "com.rdas.repo"})
//@ComponentScan
@EnableAutoConfiguration
public class TodoConfig {
}
