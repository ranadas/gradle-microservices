package com.rdas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;

/**
 * Created by rdas on 01/09/2015.
 */
//@ComponentScan({"com.rdas.controller"})
//@ComponentScan
@EnableSwagger2
@PropertySource("classpath:/application.properties")
@SpringBootApplication
@Configuration
@EnableAutoConfiguration
//@Import({TodoConfig.class})
public class SecuredRestBootExecutor {

    private static final Logger logger = LoggerFactory.getLogger(SecuredRestBootExecutor.class);

    @PostConstruct
    void initialise() {
        logger.info("\n\n ****** REST service PostConstruct\n\n");
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SecuredRestBootExecutor.class, args);
    }
}
