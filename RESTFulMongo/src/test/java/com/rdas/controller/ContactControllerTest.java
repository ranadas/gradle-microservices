package com.rdas.controller;

import com.rdas.ApplicationBootExecutor;
import com.rdas.todo.TodoRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by rdas on 27/08/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationBootExecutor.class)
@WebIntegrationTest
public class ContactControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(ContactControllerTest.class);

    @Autowired
    private TodoRepository todoRepository;
//    private ContactRepository contactRepository;


    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void run() {

    }
}