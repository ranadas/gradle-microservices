package com.rdas.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by rdas on 27/08/2015.
 */
@Api(value = "2. RestController", description = " A SIMPLE hello Controller")
@RestController
public class HelloController {

    @ApiOperation(value = "Hello.", notes = "Says Hello.")
    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public
    @ResponseBody
    String greeting() {
        return "Hello World.";
    }
}
