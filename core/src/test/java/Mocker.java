import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rdas on 01/09/2015.
 */
public class Mocker {
    /*
public class Mocker<T extends Exception> {
    private void pleaseThrow(final Exception t) throws T {
        throw (T) t;
    }

    public static void main(String[] args) {
        try {
            new Mocker<RuntimeException>().pleaseThrow(new SQLException());
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
    }
    */

    private static final List<String> NAMES = new ArrayList<String>() {{
        add("John");
        System.out.println(NAMES);
    }};

    private String name;

    public static void main(String[] args) {

        String s = new String("Hello World");

        Mocker m1 = new Mocker();
        Mocker m2 = new Mocker();

        m1.name = m2.name = "m1";
        callMe(m1, m2);
        System.out.println(m1 + " & " + m2);
        System.out.println(m1.name + " & " + m2.name);


    }

    private static void callMe(Mocker ...m) {
        m[0] = m[1];
        m[1].name="new Name";
    }
}
