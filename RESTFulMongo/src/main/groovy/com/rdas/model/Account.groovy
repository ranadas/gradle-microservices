package com.rdas.model

import org.springframework.data.annotation.Id

/**
 * Created by rdas on 28/08/2015.
 */
class Account {
    @Id
    private String id
    private String username
    private String password

    public Account() {}

    public Account(String username, String password) {
        this.username = username
        this.password = password
    }

    String getUsername() {
        return username
    }

    void setUsername(String username) {
        this.username = username
    }

    String getPassword() {
        return password
    }

    void setPassword(String password) {
        this.password = password
    }
}
