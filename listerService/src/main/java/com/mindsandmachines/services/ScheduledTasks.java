package com.mindsandmachines.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rdas on 25/08/2015.
 */
@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    // every 5 secs
    @Scheduled(fixedRate = 5000)
    public void reportCurrentTime() {
        logger.info("Time now is : " + dateFormat.format(new Date()));
    }

    // every 5 secs
    @Scheduled(cron = "*/5 * * * * ?")
    public void demoServiceMethod() {
        logger.info("With Cron time is: " + dateFormat.format(new Date()));
    }
}
