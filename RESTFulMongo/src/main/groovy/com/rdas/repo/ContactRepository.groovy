package com.rdas.repo;

import com.rdas.model.Contact;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by rdas on 26/08/2015.
 */
public interface ContactRepository extends MongoRepository<Contact, String> {}
