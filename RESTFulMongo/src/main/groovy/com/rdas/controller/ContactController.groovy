package com.rdas.controller

import com.rdas.model.Contact
import com.rdas.repo.ContactRepository
import com.rdas.todo.TodoRepository
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

/**
 * Created by rdas on 25/08/2015.
 */
@Api(value = "1. Contact Controller", description = "Operations  about Contact")
@RestController
@RequestMapping("/contacts")
class ContactController {
    private static final Logger logger = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private TodoRepository repository;

    @ApiOperation(value = "Create a hotel resource.", notes = "Returns the a list of all contacts.")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public List<Contact> getAll() {
        logger.info(" in /contacts")
        return contactRepository.findAll();
//        return new ArrayList<Contact>();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Contact create(@RequestBody Contact contact) {
        return contactRepository.save(contact);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "{id}")
    public void delete(@PathVariable String id) {
        contactRepository.delete(id);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "{id}")
    public Contact update(@PathVariable String id, @RequestBody Contact contact) {
        Contact update = contactRepository.findOne(id);
        update.setAddress(contact.getAddress());
        update.setEmail(contact.getEmail());
        update.setFacebookProfile(contact.getFacebookProfile());
        update.setFirstName(contact.getFirstName());
        update.setGooglePlusProfile(contact.getGooglePlusProfile());
        update.setLastName(contact.getLastName());
        update.setLinkedInProfile(contact.getLinkedInProfile());
        update.setPhoneNumber(contact.getPhoneNumber());
        update.setTwitterHandle(contact.getTwitterHandle());
        return contactRepository.save(update);
    }
}
